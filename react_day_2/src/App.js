import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import List from './components/List';
import ListItem from './components/ListItem';
import { bindActionCreators } from 'redux';

// import { setTodos, startLoad, endLoad } from './actions';
import * as actions from './actions';

const App = props => {

  const { addTodo, deleteTodo, setTodos, todos, startLoad, fetching, endLoad, makeActive } = props;

  useEffect(() => {
    (async () => {
      startLoad();
      const { in_progress, done } = await fetch('/todos.json').then(res =>
        res.json()
      );
      setTodos({
        in_progress,
        done,
      });
      endLoad();
    })();

    return () => { };
  }, []);

  const loading = <p>Loading...</p>;

  const renderDoneItem = ({ name, finishedTime }) => (
    <>
      <span className='badge'>
        {new Date(finishedTime).toLocaleTimeString()}
      </span>
      {name}
    </>
  );

  const renderInProgressItem = ({ isActive, id, name }) => (
    <>
      { name}
      <button type='button' className='btn btn-primary' onClick={() => makeActive(id)}>
        Start
      </button >
      <button type='button' className='btn btn-danger' style={{ display: isActive && todos.in_progress.length > 1 ? "none" : "block" }} onClick={() => deleteTodo(id)}>
        Del
      </button>
      <button type='button' className='btn btn-success' style={{ display: todos.in_progress.length === 1 ? "block" : "none" }} onClick={() => makeActive(id)}>
        Complete
      </button>
    </>
  );

  const addItem = () => {
    const txt = document.getElementById('addInput');
    if (txt.value !== '') {
      addTodo(txt.value);
      txt.value = '';
    }
  }

  return (
    <div className='container'>
      <h1>Todo React APP</h1>
      <div className='row'>
        <div className='col-xs-12'>
          <form>
            <div className='form-group'>
              <label htmlFor='addInput'>New Todo Item: </label>
              <input
                autoFocus={true}
                id='addInput'
                type='text'
                className='form-control'
                placeholder='New todo name'
              />
            </div>
            <button type='button' className='btn btn-success pull-right' onClick={addItem}>
              Add New Item
            </button>
          </form>
        </div>
      </div>
      <hr />
      <div className='row'>
        <div className='col-xs-12 col-sm-6'>
          <h3>Todos in progress</h3>
          {fetching ? (
            loading
          ) : (
            <List>
              {todos.in_progress.map(item => {
                const { id } = item;
                return (
                  <ListItem
                    key={id}
                    item={item}
                    render={renderInProgressItem}
                  />
                );
              })}
              <p>Things to do: {todos.in_progress.length}</p>
            </List>
          )}
        </div>
        <div className='col-xs-12 col-sm-6'>
          <h3>Done</h3>
          {fetching ? (
            loading
          ) : (
            <List>
              {todos.done.map(({ id, ...item }) => (
                <ListItem key={id} item={item} render={renderDoneItem} />
              ))}
              <p>Done: {todos.done.length}</p>
            </List>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  fetching: state.fetching,
  todos: state.todos,
});

const mapDispatchToProps = dispatch => {
  const { startLoad, setTodos, endLoad, addTodo, deleteTodo, makeActive } = bindActionCreators(
    actions,
    dispatch
  );

  return {
    startLoad,
    endLoad,
    deleteTodo: item => deleteTodo(item),
    addTodo: item => addTodo(item),
    setTodos: list => setTodos(list),
    makeActive: id => makeActive(id),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
