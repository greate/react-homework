import React from 'react';

export default function ListItem({ item, render }) {
  const content = render(item);

  return <li id={item.id} className='list-group-item' style={{ fontWeight: item.isActive ? "bold" : "" }}>{content}</li>;
}
