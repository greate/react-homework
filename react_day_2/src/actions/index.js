export const setTodos = todoItems => {
  return {
    type: 'SET_TODOS',
    payload: todoItems,
  };
};

export const makeActive = id => ({
  type: 'MAKE_ACTIVE',
  payload: id,
});

export const startLoad = () => ({
  type: 'FETCH_TODOS_START',
});

export const endLoad = () => ({
  type: 'FETCH_TODOS_END',
});

export const addTodo = todo => ({
  type: 'ADD_TODO',
  payload: todo
});

export const deleteTodo = todo => ({
  type: 'DELETE_TODO_ITEM',
  payload: todo
});

