const initialState = {
  fetching: true,
  todos: null,
};

const reducer = (state = initialState, action) => {
  const { todos } = state;

  switch (action.type) {
    case 'FETCH_TODOS_START':
      return { ...state, fetching: true };
    case 'FETCH_TODOS_END':
      return { ...state, fetching: false };
    case 'SET_TODOS':
      return { ...state, todos: action.payload };
    case 'MAKE_ACTIVE':
      let itemIdx = todos.in_progress.findIndex(item => item.id === action.payload);
      let previousItem = todos.in_progress.find(item => item.id === action.payload - 1);
      let newIn = todos.in_progress.filter(item => item.id !== action.payload - 1);
      const item = todos.in_progress.find(item => item.id === action.payload);

      if (previousItem === undefined) {
        previousItem = todos.in_progress.find(item => item.id === action.payload - 2);
        newIn = todos.in_progress.filter(item => item.id !== action.payload - 2);
      }

      if (todos.in_progress.length <= 1) {
        previousItem = todos.in_progress.find(item => item.id === action.payload);
        newIn = todos.in_progress.filter(item => item.id !== action.payload);
        item.isActive = true;
      }

      if (itemIdx === -1) return state;

      item.startTime = new Date().toISOString();
      item.isActive = true;

      return {
        ...state,
        todos: {
          ...todos,
          done: [...todos.done, {
            id: todos.done[todos.done.length - 1].id + 1,
            name: previousItem.name,
            isActive: false,
            finishedTime: new Date().toISOString()
          }],
          in_progress: newIn
        }
      };
    case 'DELETE_TODO_ITEM':

      const newInProgressArray = todos.in_progress.filter(item => item.id !== action.payload);

      return {
        ...state,
        todos: {
          ...todos,
          in_progress: newInProgressArray
        }
      }
    case 'ADD_TODO':
      if (action.payload === '') {
        return state;
      }

      let newId = 0;
      let isActive = todos.in_progress.length >= 1 ? false : true;

      if (todos.in_progress.length > 0) {
        newId = todos['in_progress'][todos['in_progress'].length - 1].id + 1
      }

      return {
        ...state, todos: {
          ...todos,
          'in_progress': [...todos['in_progress'], {
            id: newId,
            name: action.payload,
            isActive: isActive,
          }]
        }
      };
    default:
      return state;
  }
};

export default reducer;
