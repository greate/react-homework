import { combineReducers } from 'redux'

import planetsReducer from "./planets";
import filmsReducer from "./films";

export default combineReducers({
  planets: planetsReducer,
  films: filmsReducer,    
});
