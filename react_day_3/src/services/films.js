const apiHost = process.env.REACT_APP_SW_API_HOST;

class FilmsApiService {

  getFilms = async page => fetch(`${apiHost}/films/?page=${page}`);

  getFilm = async id => fetch(`${apiHost}/films/${id}/`);

  getStarship = async id => fetch(`${apiHost}/starships/${id}/`);
}

export default new FilmsApiService();
