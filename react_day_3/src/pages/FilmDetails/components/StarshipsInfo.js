import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import useStarship from '../../../hooks/useStarship';
import { getStarshipImageUrl } from '../../../utils';
import FilmInfoRow from '../../Films/components/FilmInfoRow';

export const StarshipWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  img {
    margin-bottom: 10px;
    height: 110px;
    width: 80px;
  }
`;

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 6px;
`;

export default function StarshipInfo({ id }) {
  const info = useStarship(id);
  const starshipURL = useMemo(() => getStarshipImageUrl(id), [id]);

  return info ? (
    <StarshipWrapper>
      <img src={starshipURL} alt={info.name} className='img-rounded' />
      <Link to={`/starships/${id}`}>{info.name}</Link>
      <InfoWrapper>
        <FilmInfoRow name='Model' value={info.model} />
      </InfoWrapper>
      <InfoWrapper>
        <FilmInfoRow name='Cost' value={info.cost_in_credits} />
      </InfoWrapper>
      <InfoWrapper>
        <FilmInfoRow name='Max Atmosphering Speed' value={info.max_atmosphering_speed} />
      </InfoWrapper>
    </StarshipWrapper>
  ) : (
    ''
  );
}
