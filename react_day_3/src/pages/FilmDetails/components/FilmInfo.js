import { useCallback } from "react";
import styled from "styled-components";
import { getDefaultImage, getFilmImageUrl } from "../../../utils";
import FilmInfoRow from "../../Films/components/FilmInfoRow";

const defaultImageUrl = getDefaultImage();

const InfoWrapper = styled.div`
  display: flex;
  padding: 20px;
  font-size: 16px;
  background-color: #2e3338;
  border-radius: 10px;
  flex-direction: row;
  column-gap: 30px;
  height: 250px;
  img {
    max-height: 700px;
  }
  div:nth-child(n + 1) {
    display: flex;
    flex-direction: column;
    gap: 15px;
    dl {
      display: flex;
      flex-direction: row;
      gap: 15px;
      margin: 0;
    }
  }
`;

export default function FilmInfo({ id, film }) {
  const onImageLoadError = useCallback((e) => {
    if (e.target.src !== defaultImageUrl) {
      e.target.src = defaultImageUrl;
    }
  }, []);

  return (
    <InfoWrapper>
      <div>
        <img
          src={getFilmImageUrl(id)}
          alt={film.name}
          className="img-thumbnail"
          onError={onImageLoadError}
        />
      </div>
      <div>
        <dl>
        <FilmInfoRow name='Episode Id: ' value={`${film.episode_id}`} />
        </dl>
        <dl>
        <FilmInfoRow name='Opening Crawl: ' value={`${film.opening_crawl}`} />
        </dl>
        <dl>
        <FilmInfoRow name='Director: ' value={`${film.director}`} />
        </dl>
        <dl>
        <FilmInfoRow name='Producer: ' value={`${film.producer}`} />
        </dl>
      </div>
      </InfoWrapper>
  );
}
