import { useContext, useEffect, useReducer, useState } from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Loader from 'components/Loader';
import ServerError from 'components/ServerError';

import apiService from 'services/films';

import { NotificationContext } from '../../components/Notification';
import {
  InfoWrapper,
  StarshipWrapper as Sw,
} from './components/StarshipsInfo';
import FilmInfo from './components/FilmInfo';
import { getStarshipId, getStarshipImageUrl } from '../../utils';
import withFilms from '../../hoc/withFilms';
import { errorFetch, startFetch, successFetch } from './state/actions';
import reducer from './state/reducer';
import FilmInfoRow from '../Films/components/FilmInfoRow';

const StarshipWrapper = styled.div`
  display: flex;
  gap: 30px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
`;

const NextFilmLinkWrapper = styled.div`
  float: right;
  display: inline-block;
  line-height: 39px;
  a {
    text-decoration: none;
  }
`;


const initialState = {
  fetching: false,
  error: false,
  filmInfo: null,
};

const FilmDetails = ({ getData }) => {
  const { id } = useParams();

  const notificationContext = useContext(NotificationContext);
  const [state, dispatch] = useReducer(reducer, initialState);
  const [starshipInfo, setStarshipInfo] = useState([]);
  const { fetching, error, filmInfo } = state;

  useEffect(() => {
    (async () => {
      dispatch(startFetch());

      try {
        const info = await getData(id).then(res => res.json());
        notificationContext.showInfo(`${info.title} Fetched`);

        dispatch(successFetch(info));
      } catch {
        dispatch(errorFetch());
      }
    })();
  }, [id]);

  useEffect(() => {
    if (!filmInfo?.starships) return;

    const starshipPromises = filmInfo?.starships.map(url => {
      const starshipId = url.replace(/^[\D]+|\/$/g, '');

      return apiService.getStarship(starshipId).then(res => res.json());
    });

    (async () => {
      let starshipFull = await Promise.allSettled(starshipPromises);

      starshipFull = starshipFull
        .map(({ status, value }) => {
          return status === 'fulfilled' ? value : null;
        })
        .filter(info => info);

      setStarshipInfo(starshipFull);
    })();
  }, [filmInfo?.starships]);

  if (fetching) return <Loader />;
  if (error) return <ServerError />;
  if (!filmInfo) return '';

  return (
    <div>
      <h1>
      {starshipInfo.name}
      <NextFilmLinkWrapper>
      <Link to={`/films/${+id + 1}`} style={{display: id >= 6 ? "none" : "block"}}>&rarr;</Link>
      </NextFilmLinkWrapper>

        <small style={{ fontSize: 12, paddingLeft: 20 }}>
          <NotificationContext.Consumer>
            {value => (value.message ? `MESSAGE: ${value.message}` : '')}
          </NotificationContext.Consumer>
        </small>
      </h1>
      <FilmInfo id={id} film={filmInfo} />

      {starshipInfo.length > 0 && (
        <>
          <h2>Starships</h2>
          <StarshipWrapper>
            {starshipInfo.map(info => {
              const id = getStarshipId(info.url);
              const starshipURL = getStarshipImageUrl(id);

              return (
                <Sw key={id}>
                  <img
                    src={starshipURL}
                    alt={info.name}
                    className='img-rounded'
                  />
                  <Link to={`/starships/${id}`}>{info.name}</Link>
                  <InfoWrapper>
                    <FilmInfoRow name='Model' value={info.model} />
                  </InfoWrapper>
                  <InfoWrapper>
                    <FilmInfoRow name='Cost' value={info.cost_in_credits} />
                  </InfoWrapper>
                  <InfoWrapper>
                    <FilmInfoRow name='Max Atmosphering Speed' value={info.max_atmosphering_speed} />
                  </InfoWrapper>
                </Sw>
              );
            })}
          </StarshipWrapper>
        </>
      )}
      </div>
  );
};

export default withFilms(FilmDetails);
