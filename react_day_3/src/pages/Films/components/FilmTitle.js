import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import FilmInfoRow from './FilmInfoRow';

const FilmInfoWrapper = styled.div`
  dt {
    width: 94px;
  }
  dd {
    margin-left: 111px;
  }
`;

export default function FilmTitle({ item, onImageLoadError }) {
  console.log(item)
  return (
    <div className='thumbnail film-image'>
      <img src={item.imgSrc} alt={item.name} onError={onImageLoadError} />
      <div className='caption'>
        <h3>
          <Link to={`/films/${item.id}`}>{item.title}</Link>
        </h3>
        <FilmInfoWrapper>
          <dl className='dl-horizontal'>
            <FilmInfoRow name='Episode Id' value={item.episode_id} />
            <FilmInfoRow name='Release date' value={item.release_date} />
            <FilmInfoRow name='Director' value={item.director} />
          </dl>
        </FilmInfoWrapper>
      </div>
    </div>
  );
}
