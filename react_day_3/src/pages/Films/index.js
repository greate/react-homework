import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/macro';

import { loadFilms } from 'actions/films';

import Loader from 'components/Loader';
import ServerError from 'components/ServerError';
import FilmTitle from './components/FilmTitle';
import { getDefaultImage, getFilmId, getFilmImageUrl } from '../../utils';

const defaultImageUrl = getDefaultImage();

const FilmsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 30px;
  align-items: stretch;
  align-content: flex-start;
  justify-content: flex-start;
  > div {
    flex-grow: 1;
    width: 300px;
    &:last-of-type {
      flex-grow: 0;
    }
  }
`;

const Films = () => {
  const dispatch = useDispatch();
  const { films, fetchingFilms, filmsError } = useSelector(
    state => state.films
  );

  const [page, setPage] = useState(1);

  const onImageLoadError = useCallback(e => {
    if (e.target.src !== defaultImageUrl) {
      e.target.src = defaultImageUrl;
      e.target.classList.add('img-placeholder');
    }
  }, []);

  const { results: list } = films || {};

  useEffect(() => {
    dispatch(loadFilms(page));
  }, [page, dispatch]);

  return (
    <div>
      <h1>Star Wars Films</h1>

      {fetchingFilms && <Loader />}

      {filmsError && <ServerError />}
      {!filmsError && !fetchingFilms && list && (
        <>
          <FilmsWrapper>
            {list.map(item => {
              const cloneItem = { ...item };
              cloneItem.id = getFilmId(item.url);
              cloneItem.imgSrc = getFilmImageUrl(cloneItem.id);
              return (
                <FilmTitle
                  key={cloneItem.name}
                  item={cloneItem}
                  onImageLoadError={onImageLoadError}
                />
              );
            })}
          </FilmsWrapper>
        </>
      )}
    </div>
  );
};

export default Films;
