import React from "react";
import filmsApiService from "services/films";

const withFilms = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => {
    return (
      <WrappedComponent {...props} getData={filmsApiService.getFilm} />
    );
  };

  return hocComponent;
};

export default withFilms;
